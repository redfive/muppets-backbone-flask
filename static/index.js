var MuppetModel = Backbone.Model.extend({
  defaults: {
    id: null,
    name: null,
    occupation: null
  }
});

var MuppetsCollection = Backbone.Collection.extend({
  url: '/muppets',
  model: MuppetModel,

  parse: function(data) {
    return data.objects;
  }
});


// View class for each individual muppet item
var MuppetsListItemView = Backbone.View.extend({
  tagName: 'li',
  className: 'muppet',
  template: _.template($('#muppets-item-tmpl').html()),

  initialize: function() {
    this.listenTo(this.model, 'destroy', this.remove);
  },

  render: function() {
    // pull the template out of the page and render it
    var html = this.template(this.model.toJSON());
    this.$el.html(html)
    return this;
  }
});

// View class for the list of all muppets
var MuppetsListView = Backbone.View.extend({
  // This view has a root element with id muppets-app
  el: '#muppets-app',

  initialize: function() {
    // when the collection updates, kick off a render
    this.listenTo(this.collection, 'sync', this.render);
  },

  render: function() {
    // clear the content
    var $list = this.$('ul#muppets-list').empty();

    // for each item in the collection, add an item to the list
    this.collection.each(function(model) {
      var item = new MuppetsListItemView({model: model});
      $list.append(item.render().$el);
    }, this);

    // make chaining possible
    return this;
  }
});

var muppetsList = new MuppetsCollection();
var muppetsView = new MuppetsListView({collection: muppetsList});
muppetsList.fetch()

