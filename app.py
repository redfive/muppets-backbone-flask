#!/usr/bin/env python

from flask import Flask, render_template
from restless.fl import FlaskResource

app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')

#TODO: figure out how to break this out into a separate api.py file; the sample in the
#      restless docs didn't work as written. For a flask app where does api.py get 
#      imported?
class MuppetResource(FlaskResource):

  # hand back an iterable of the type of things this is a resource for
  def list(self):
    return [ { "id": 1, "name": "Kermit", "occupation": "Lover"},
             { "id": 2, "name": "Fozzy",  "occupation": "Comedian"} ]

  # hand back one of the things this is a resource for
  def detail(self, pk):
    return { "id": 1, "name": "Kermit", "occupation": "Dreamer"}

# connect the api to the prefix -- should have an api version in there
MuppetResource.add_url_rules(app, rule_prefix='/muppets/')

if __name__ == "__main__":
    app.debug = True
    app.run()
